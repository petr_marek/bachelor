> set.seed(1)
> data = rnorm(150)
> x = seq(-3.1, 3.1, 0.001)
> bws = c(1.5, 1, 0.5, 0.25, 0.125)
> plot(x,	dnorm(x), ylim=c(0, 0.55),
+ 	type = 'l', lwd = 2)
> for (i in seq_along(bws))
+ 	lines(
+ 		density(data, bw = bws[i]),
+ 		col = i+1, lty = 2
+ 	)
> legend('topright',
+ 	lty = c(1, rep(2, length(bws))),
+ 	col = 1:(length(bws)+1),
+ 	legend = c('dnorm', bws)
+ )
> rug(data)

