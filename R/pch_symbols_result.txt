> seq = 0:18
> x = rep((1:4)*20, 5)
> y = c()
> for (i in rev(1:5))
+ 	y = c(y, rep(i, 4))
> x = x[seq + 1]
> y = y[seq + 1]
> 
> plot(x, y, pch = seq,
+ 	xaxt = 'n', yaxt = 'n', main = NULL,
+ 	frame = FALSE, xlim = c(10, 90),
+ 	ylim = c(0, 5.5)
+ )
> text(x, y - 0.45, seq, cex = 0.7)

