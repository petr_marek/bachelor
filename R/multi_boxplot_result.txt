> data = read.csv('data/car_fuel.csv')
> uq = unique(data$Cylinders)
> uq = sort(uq[!is.na(uq)])
> cc.lph =
+ 	data$LPH[complete.cases(data$LPH)]
> # prazdny graf
> plot.new()
> plot.window(
+ 	xlim = c(2, max(uq)+1),
+ 	ylim = c(0.9*min(cc.lph),
+ 		1.1*max(cc.lph))
+ ); axis(2)
> boxplot(
+ 	LPH ~ Cylinders,
+ 	data = data, at = uq, add = TRUE
+ )
> abline(
+ 	lm(LPH ~ Cylinders, data = data),
+ 	col = 'red'
+ )
