> histHoriz = function(data, breaks = 'Sturges', ...) {
+ 	# histogram nevykreslime
+ 	h = hist(data,
+ 		plot = FALSE,
+ 		breaks = breaks
+ 	)
+ 	# ale vykreslime sloupcovy graf
+ 	barplot(h$density,
+ 		space = 0, horiz = TRUE,
+ 		col = NA, axes = FALSE
+ 	)
+ }
> 
> histHoriz(rnorm(500))
