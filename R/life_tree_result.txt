> load('R/plot.lifeTree.R')
> data = read.table(
+ 	'data/cz_pop.data',
+ 	header = TRUE
+ )
> plot.lifeTree(
+ 	data$muzi,
+ 	data$zeny,
+ 	data$vek
+ )
