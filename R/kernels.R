setwd('/home/petr/Dropbox/School/bakalarka')
setEPS()
postscript('images/kernels.eps')
kernels =
	eval(formals(density.default)$kernel)
plot(density(0, bw = 1), xlab = "",
	main = "Jadra v R pro sirku vyhlazovaciho okna = 1",
	ylim = c(0, 0.5))
for(i in 2:length(kernels))
	lines(
		density(0, bw = 1,
			kernel =  kernels[i]),
		col = i
	)
legend('topright', legend = kernels,
	col = seq_along(kernels), lty = 1,
	cex = 1.2
)
dev.off()