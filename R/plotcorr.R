setwd('/home/petr/Dropbox/School/bakalarka')
library('ellipse')
data = mtcars[,
	!(names(mtcars) %in% c('vs','am'))]
setEPS()
postscript('images/plotcorr.eps')
# cex.lab pro vetsi pismo
plotcorr(cor(data), cex.lab = 1.5)
dev.off()