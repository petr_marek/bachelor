setwd('/home/petr/Dropbox/School/bakalarka')
seq = rev(1:6)
mar = 20

setEPS()
postscript('images/line_types.eps', width = 3, height = 3.25)
par(mar = c(0,3,0,3))  # odstrani okraje

y = cbind(seq*mar, seq*mar)
x = cbind(seq*0, seq*0 + 2)

plot(x, y, type = 'n', main = NULL, 
	xaxt = 'n', yaxt = 'n',
	ylim = c(0, 130), frame = FALSE)

for (i in seq)
	lines(x[i,], y[i,], lty = i, lwd = 2)

text(x = mean(x[1,]), y = y - 8,
	labels = rev(seq))

dev.off()
