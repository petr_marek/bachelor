setwd('/home/petr/Dropbox/School/bakalarka')
data = read.csv('data/car_fuel.csv')
setEPS()
postscript('images/boxplot_with_mean.eps')
par(mar = c(1,3,1,1))
boxplot(data$LPH)
# na.rm smaze NA (prazdne) zaznamy
prumer = mean(data$LPH, na.rm = TRUE)
# pro symbol krizku volime pch = 3
points(prumer, pch = 3)

dev.off()
