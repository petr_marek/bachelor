> plot.lifeTree = function(a, b, names = c()) {
+ 	# jen stejna struktura dat
+ 	if (length(a) != length(b))
+ 		return(FALSE)
+ 	# relativni cisla
+ 	sum.ab = sum(c(a,b))
+ 	a = a/sum.ab
+ 	b = b/sum.ab
+ 	# osa y
+ 	if (!length(names))
+ 		names = 1:length(a)
+ 	# pomocne promenne
+ 	data.min = min(c(a,b))
+ 	data.max = max(c(a,b))
+ 	data.length = length(a)
+ 	data.seq = 1:length(a)
+ 	data.col = c('#4682AB', '#AB4650')
+ 	# okraje
+ 	# par(mar = c(2,3,2,0))
+ 	# prazdny graf
+ 	plot(
+ 		NA,
+ 		xlim = c(-data.max, data.max),
+ 		ylim = c(names[1], names[length(names)]),
+ 		xlab = 'Podil na populaci',
+ 		ylab = 'Vek'
+ 	)
+ 	# cary
+ 	rect(-a, data.seq, 0, data.seq + 1.5, col = data.col[1], border = 'NA')
+ 	rect(b, data.seq, 0, data.seq + 1.5, col = data.col[2], border = 'NA')
+ 	# legenda
+ 	legend('topright',
+ 		col = data.col,
+ 		legend = c('muzi', 'zeny'),
+ 		lwd = 3
+ 	)
+ }
