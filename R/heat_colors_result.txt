> # balicek pro obdelnik s prechodem
> library('plotrix')
> # colorRampPalette vytvori funkci,
> # kterou budeme moci pouzivat
> heat_colors = colorRampPalette(
+ 	c('#0101AD', '#E5E03B', '#FF1000')
+ )
> 
> # prazdny graf
> plot(c(0,300), c(0,300),
+ 	type = 'n', main = NULL,
+ 	xaxt = 'n', yaxt = 'n')
> # obdelnik s prechodem barev
> gradient.rect(0, 0, 300, 300,
+ 	col = heat_colors(300))
