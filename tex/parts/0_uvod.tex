%!../bakalarka.tex
\section{Předmluva} % (fold)
\label{sec:p_edmluva}
	Představme si, že jsme do rukou dostali data, o~kterých se chceme něco dozvědět. Než je začneme jakkoliv dále analyzovat, je vhodné si data nějakým způsobem prohlédnout. Spousta vlastností je totiž nejlépe vidět z~grafů. Je možné, že u~některých dat pomocí grafů zjistíme, že jejich rozdělení neumožňuje určitý druh analýzy, nebo je pro něj krajně nevhodné.

	Existuje celá řada grafů, které umožňují primárně poznat data a~pomoci s~(až druhotnou) analýzou. Z~relevantních grafů můžeme získat veškeré potřebné informace o~tom, jakou část statistického aparátu bychom měli nasadit. Grafy odhalují nesrovnalosti v~datech a pomáhají pro ověření předpokladů pro další postup.

	Celá práce je psaná v~duchu prvotního poznání dat grafickými nástroji. Pro tvorbu grafů je použito programové prostředí R~\cite{rdoc}, které v~sekci~\myRef{sec:uvod_do_r} stručně představím. Znalost R~však není pro čtení práce nutná. Po teoretickém úvodu by se měl uživatel zvládnout orientovat v~komentovaném kódu\footnote{Vždy když poprvé použiji některou z~pokročilejších či méně známých funkcí, příkládám komentář, popřípadě odkaz do sekce, kde se funkcí přímo zabývám.}. Při tvorbě grafu je kód vždy uveden hned vedle  grafu tak, že i~méně zkušený čtenář zvládne takový graf napodobit a~dále přizpůsobit.

	Hlavním obsahem práce je představení grafů, a~to jak pro jednu proměnnou v~sekci~\myRef{sec:1d}, tak pro dvě proměnné v~sekci~\myRef{sec:2d} a~pro tři a~více proměnných v~sekci~\myRef{sec:3d}. Graf představím, popíši jaké má výhody a~nevýhody a~ukáži jeho implementaci v~R. Dále uvedu, k~čemu je graf vhodné použít a~co vše nám může o~datech ukázat. Zmíním se také o~případných nedostatcích. Informace o~většině méně známých grafů, kterými se budu zabývat, jsou dostupné jen v~anglické literatuře. Názvy těchto grafů se nesnažím otrocky překládat, ale držím se originálních anglických názvů, jak je standardem.

	Rozhodl jsem se do sekce~\myRef{sec:nastaveni_grafu} vypsat některé univerzální postupy a~k~nim náležící funkce používané při tvorbě grafů v~R, které mají pomoci zejména nezkušenému čtenáři. V~sekci~\myRef{sec:vytvorene_funkce} jsem shrnul zajímavé a~užitečné funkce, které jsem při psaní této práce vytvořil.

	Pro ilustraci grafů jsem se vždy snažil vybrat co možná nejvhodnější data. Datových souborů je proto hned několik. V~textu se na ně odkazuji kurzívou jako například \dRef{car_fuel}. Seznam datových souborů a~informace o~nich jsem zahrnul do přílohy.
% section p_edmluva (end)

\section{Úvod do R} % (fold)
\label{sec:uvod_do_r}
	\subsection{O systému R} % (fold)
	\label{sub:o_r}
		R~je volně šířitelné programové prostředí\footnote{Tvůrci R~jej nevidí primárně jako statistický program, jak je občas prezentován. Preferují označení programové prostředí, ve kterém jsou implementovány statistické postupy~\cite{rabout}.} podporující velkou škálu operačních systémů - lze jej spustit na *nix systémech (všechny Linux distribuce, MacOSX) i~na operačním systému Windows. R~je distribuováno jako základní jádro, které je možné rozšiřovat uživateli spravovanými balíčky, které významně rozšiřují jeho možné využití.

		Jedná se o~velmi mocný nástroj s~množstvím grafických výstupů, které umožňuje velmi detailně přizpůsobovat. Umí také generovat celou řadu dalších výstupů.

		R~je také programovací jazyk, se~kterým programové prostředí R~pracuje. Syntaxí je podobný pythonu. Dle tabulky nejpoužívanějších programovacích jazyků (sestavuje společnost Tiobe Software\footnote{Celá tabulka je k~nahlédnutí na \href{http://www.tiobe.com/index.php/content/paperinfo/tpci/index.html}{http://www.tiobe.com/index.php/content/paperinfo/tpci/index.html}.}) je jazyk R~za březen 2013 na 26. místě. Nutno podotknout, že se jedná o~všechny programovací jazyky, nikoliv jen statistické (z těch se ještě na 23. místě umístil SAS, jinak žádný). Je tedy vidět, že R~je velmi populární a~široce používaný, a~to především v~zahraničí. Aktuální verze (v době psaní) je \textit{3.0}.
	% subsection o_r (end)
	\subsection{Práce s~R} % (fold)
	\label{sub:prace_s_r}
		V~R je možné pracovat rovnou v~konzoli, nebo psát R~skripty. Konzoli je možné spustit příkazem \code{R} na příkazové řádce:

		\embedCode{outputs/R_startup.txt}

		Speciální znak \code{>} uvozuje řádek a~je možné za ním psát příkazy\footnote{Stejně jako za \code{\$} na *nixu.}. Jazyk R~má velmi jednoduchou a~intuitivní syntax. Lze ho vlastně použít i~jako obyčejnou kalkulačku bez jakékoliv předchozí znalosti:\\
		\multiCode{XX}{
			\embedR{math_1} & \embedR{math_2}
		}\\
		Pro tvorbu grafů budeme používat funkce. Funkce voláme jejich jménem společně s~argumenty v~kulatých závorkách. Například \code{c()} pro tvorbu vektoru a~\code{sum()} pro vypočtení sumy:

		\embedR{basic_functions}

		S~R se velmi snadno začíná, jelikož obsahuje \textbf{velmi kvalitní} nápovědu. Pro nápovědu ohledně specifické funkce stačí napsat \code{?jmenoFunkce}, např. tedy \code{?sum}. Tato nápověda obsahuje detailní popis parametrů, včetně vysvětlení a~příkladů. Kolem R~je velmi silná a~přátelská komunita\footnote{Za což R, stejně jako za svůj velký rozmach, vděčí především Linuxu~\cite{fox}.}, takže není problém se při případných potížích obrátit na zkušenější uživatele na internetu\footnote{Asi nejlepší volbou je stránka \href{http://stackoverflow.com/}{http://stackoverflow.com/}.}.

		Během práce v~R se nevyhneme použití externích balíčků. Instalaci externího balíčku lze provést pomocí příkazu \code{install.packages('jmenoBalicku')}. To stačí provést jednou. Balíček se stáhne a~bude nadále k~dispozici. Pro využití funkcionality tohoto balíčku je potřeba jej ve skriptu zahrnout pomoci \code{library('jmenoBalicku')}.

		Balíčků je celá řada. Pro vyhledávání je vhodné použít Google a~zadat relevantní klíčová slova (např. \uv{R violin plot package}) a~nebo vyhledávat na stránce se seznamem všech evidovaných balíčků\footnote{\href{http://cran.r-project.org/web/packages/available\_packages\_by\_name.html}{http://cran.r-project.org/web/packages/available\_packages\_by\_name.html}}.
	% subsection prace_s_r (end)
	\subsection{Grafy v~R} % (fold)
	\label{sub:grafy_v_r}
		Nejpřesnější popis tvorby grafů si vypůjčím od pánů Foxe s~Weisbergem \cite{fox}. Tvorbu grafů v~R přirovnávají k~reálnému kreslení grafů na papír. V~reálu většinou totiž začneme tak, že si načrtneme osy a~určíme měřítko. Poté přidáváme jednotlivé prvky grafu, jako jsou křivky, čáry, body, nadpisy či legenda. Přesně tak se to dělá v~R - jen místo ořezané tužky používáme R~příkazy. 
	% subsection grafy_v_r (end)
% section uvod_do_r (end)