%!../bakalarka.tex
\section{Jednorozměrná data} % (fold)
\label{sec:1d}
	Jako jednorozměrná data označujeme taková data, která zachycují hodnoty jedné proměnné. Pro potřeby grafického zobrazování rozdělíme proměnné na kvantitativní a~kategoriální. Podle charakteru proměnné použijeme různé typy grafů.

	Pro kvantitavní proměnné budeme zkoumat především míry polohy (aritmetický průměr, medián, modus a~kvantily), míry variability (rozptyl, respektive směrodatná odchylka a~mezikvartilové rozpětí) a~míry asymetrie (šikmost a~špičatost). Tyto míry se pokusíme graficky zobrazit. Budeme zkoumat, zda-li se v~datech vyskytují nějaká odlehlá pozorování\footnote{Tedy taková pozorování, jejichž hodnoty se zdají být v~numerickém nesouladu se zbytkem dat~\cite{outlierboxplot}.} a~\mbox{zda-li} jsou rozdělení těchto proměnných jednovrcholová. Využijeme při tom krabičkový graf, histogram a~QQ graf. V~případě spojitých kvantitativních proměnných se také blíže podíváme na jejich rozdělení, pro která vykreslíme jádrový odhad. U~kategoriálních proměnných budeme zkoumat četnost, kterou znázorníme výsečovým grafem a~wordcloudem.

	Především pro základní popis proměnných platí otřepané \uv{obrázek vydá za tisíc slov}. Grafy pro jednu proměnnou jsou lehce pochopitelné i~úplným laikem. Grafy mu umožní na data nahlédnout a~bez znalosti výpočtu statistik je popsat.

	\subsection{Krabičkový graf} % (fold)
	\label{sub:krabi_kov_graf}
		Krabičkový graf je jedním z~nejjednodušších způsobů, jak zobrazit základní informace o~kvantitativní proměnné. Sestává z~krabičky, \uv{vousů} a~jednotlivých bodů. Hranice krabičky jsou vymezeny dolním a~horním kvartilem. Krabička je protnuta silnou linkou, jejíž poloha je určena hodnotou mediánu. Z~krabičky vystupují oběma směry \uv{vousy} znázorňující úrovně nevychýleného\footnote{Tj. maxima a~minima z~pozorování, která nejsou označena jako odlehlá.} maxima a~minima~\cite{boxplot}. Pomocí bodů jsou znázorněny odlehlé hodnoty\footnote{Nejčastěji (v~R pak ve výchozím nastavení) se za odlehlou hodnotu považuje taková hodnota, která je \uv{vzdálena} od hrany krabičky (tj. od dolního, respektive horního kvartilu) o~více než jedenapůlnásobek mezikvartilového rozpětí~\cite{rdoc}.}.

		Krabičkový graf vykresluje hodnoty základních měr polohy. Vyčteme z~něj také informace o~variabilitě díky jasně viditelné hodnotě mezikvartilového rozpětí~\cite{violinplot}. Z~krabičkového grafu se ovšem nedozvíme, jakých hodnot nabývají míry asymetrie. Asymetrii však můžeme odhalit na základě vykreslených odlehlých pozorování. Krabičkový graf je vhodné doplnit dokreslením průměru, abychom měli přesnější představu o~jeho vzdálenosti od mediánu. Výraznější vzdálenost totiž může poukazovat na kladné či záporné zešikmení~\cite{bpplot}.

		% \ir{boxplot_default}{Krabičkový graf ve výchozím nastavení}
		V~R je možné krabičkový graf vykreslit funkcí \code{boxplot()}. Pro dokreslení průměru můžeme použít funkci \code{points()}\footnote{Více o~této funkci píši v~sekci~\myRef{sub:pridani_bodu}.}. Ukažme si krabičkový graf ve výchozím nastavení (s~přidaným křížkem pro průměr) pro proměnnou spotřeba na 100 kilometrů z~dat \dRef{car_fuel} na obrázku~\iRef{boxplot_with_mean}. 

		\ir{boxplot_with_mean}{Krabičkový graf s~křížkem v~místě průměru}

		R~nabízí mnoho možností, jak krabičkový graf přizpůsobit. Do tabulky~\tRef{boxplot} jsem vypsal užitečné parametry, kterými můžeme některé z~přizpůsobení provést. Tyto parametry se nám budou hodit především později v~sekci~\myRef{sub:multiple_boxplot} při práci s~vícenásobným krabičkovým grafem. Kombinací těchto parametrů můžeme pro stejnou proměnnou získat například krabičkový graf na obrázku~\iRef{boxplot_customized}. 

		\attributesList{boxplot}{tvorbu krabičkového grafu}{
			\code{horizontal} & \code{FALSE} &
				pokud je \code{TRUE}, tak se krabičkový graf vykreslí na šířku, ne na výšku\\
			\hline
			\code{range}      & \code{1.5} &
				nastavení \uv{délky} krabičkového grafu; určuje, kolika-násobek mezikvartilového rozpětí je hranicí pro odlehlá pozorování \\
			\hline
			\code{varwidth}   & \code{FALSE} &
				pokud je \code{TRUE}, tak jsou \uv{krabičky} v~grafu široké jako druhá odmocnina počtu pozorování v~této skupině \\
		}

		\ir{boxplot_customized}{Přizpůsobený krabičkový graf}

		Při grafické analýze je vhodné zkoumat data i~pomocí jiných grafů. Jelikož je krabičkový graf jakýmsi kompromisem mezi jednoduchostí zobrazení a~reflektováním maxima informací o~proměnné\footnote{To se dá zobecnit víceméně pro všechna grafická zobrazení. Většinou totiž graficky zobrazujeme v~méně rozměrech (v drtivé většině ve dvou) než je počet proměných~\cite{sectioned_density}.}, je potřeba dávat pozor při porovnávání různých rozdělení. Krabičkový graf někdy zobrazí velmi odlišná rozdělení velmi podobně~\cite{sectioned_density}. Příkladem mohou být vygenerovaná data na obrázku~\iRef{boxplot_vs_reality}, kde jsem pro porovnání přidal histogramy\footnote{V kódu je použita funkce \code{layout()} - více o~této funkci v~\myRef{ssub:v_ce_graf_v_jednom_obr_zku}. Funkce pro tvorbu histogramu \code{hist()} je společně s~grafem jako takovým popsána v~\myRef{sub:histogram}.}.

		\newpage

		Krabičkový graf je velmi užitečným nástrojem při základním popisu zkoumanné proměnné. Není to však jeho jediné využití. Jak uvidíme v~sekci~\myRef{sub:multiple_boxplot}, lze krabičkový graf snadno použít také pro zkoumání závislostí mezi kvantitativní a~kategoriální proměnnou\footnote{Dokonce je to jeden z~nejlepších nástrojů~\cite{boxplot}.}. Krabičkový graf se pro množství informací, které obsahuje, stal základem dalších (pokročilejších) grafů, jako jsou například violin plot či bagplot\footnote{Více v~sekci \myRef{sub:violin_plot}, resp. \myRef{sub:bagplot}.}. Při porovnávání různých rozdělení je potřeba dávat pozor, jelikož použití krabičkového grafu může být poněkud zavádějící, jak je vidět na obrázku~\iRef{boxplot_vs_reality}.

		\ir{boxplot_vs_reality}{Pomocí krabičkového grafu mnohdy nedokážeme rozeznat různé tvary rozdělení}
		% \subsubsection{Vícenásobný krabičkový graf} % (fold)
		% \label{ssub:v_cen_sobn_krabi_kov_graf}
		% 	V~R lze jednoduše vytvořit celou sérii krabičkových grafů. Vykresleme například do obrázku~\iRef{multi_boxplot} krabičkové grafy pro spotřebu rozdělené podle kategoriální proměnné počet válců z~dat \dRef{car_fuel}.

		% 	\embedR{multi_boxplot}
		% 	\img{multi_boxplot}{Vícenásobný krabičkový graf}

		% 	Z~grafu je krásně patrná přímá závislost. Tuto informaci nám podtrhuje možnost vidět mezikvartilové rozpětí stejně tak jako minimum a~maximum. Koukáme se tedy na jakési \textit{nevychýlené znázornění} závislosti.

		% 	Pro tento druh grafu lze použít všechny zajímavé atributy pro nastavení jednoduchého krabičkového grafu, shrnuté v~tabulce \myRef{tab:boxplot}.

		% % subsubsection v_cen_sobn_krabi_kov_graf (end)
	% subsection krabi_kov_graf (end)

	\clearpage

	\subsection{Histogram} % (fold)
	\label{sub:histogram}
		Histogram patří mezi nejužívanější grafy pro popis jedné proměnné, jejíž rozdělení graficky znázorňuje pomocí sloupcového diagramu se stejně širokými sloupci~\cite{dalgaard}. Šířka sloupců vyjadřuje šířku intervalů, do kterých jsou hodnoty proměnné rozděleny. Výška sloupce pak znázorňuje četnosti těchto hodnot v~jednotlivých intervalech.

		V~R se pro tvorbu histogramu používá funkce \code{hist()}. Výchozí nastavení histogramu pro náhodný výběru z~normálního rozdělení vidíme na grafu~\iRef{histogram_default}.

		\ir{histogram_default}{Histogram ve výchozím nastavení}

		Při tvorbě histogramu je potřeba zvážit, do kolika intervalů budeme hodnoty proměnné dělit, respektive jaká bude šířka těchto intervalů. Volba šířky je naprosto zásadní a~její důležitost roste s~rozsahem souboru. Při špatně zvolené šířce intervalů nám histogram dokáže výrazně zkreslit představu o~datech. Možností, jak volit šířku intervalu je hned několik. Výchozím nastavením pro funkci \code{hist()} je \mbox{Sturgesovo} pravidlo, které je jedním z~prvních pravidel pro volbu šířky\footnote{Ve skutečnosti se jedná spíše o~pravidlo pro počet skupin, jak píše Scott\cite{scott}.} intervalů vůbec~\cite{Wand96data-basedchoice}. Sturges navrhuje volit počet intervalů $k$ jako:
		\begin{equation}\label{eq:sturges_k}
			k = 1 + \log_2 n
		\end{equation}
		kde $n$ je rozsah výběru. Šířku intervalu z~\myRef{eq:sturges_k} můžeme vyjádřit jako:
		\begin{equation}
			h_{st} = \frac{R}{1 + \log_2 n}
		\end{equation}
		kde:

		\definitionList{5em}{
			$n$ & rozsah výběru,                             \\  
			$R$ & variační rozpětí, tj. $x_{max} - x_{min}$. \\  
		}

		Sturgesovo pravidlo není vhodné pro velké výběry, které má tendenci příliš vyhlazovat použitím nedostatečného počtu intervalů~\cite{scott}, jak uvidíme dále. Alternativou může být pravidlo Scottovo:
		\begin{equation}\label{eq:scott_h}
			h_{sc} = \frac{3.5 \cdot \hat \sigma}{n^{1/3}}
		\end{equation}
		nebo Freedman-Diaconisovo:
		\begin{equation}\label{eq:fd}
			h_{fd} = 2 \cdot \frac{IQR(x)}{n^{1/3}},
		\end{equation}
		kde:

		\definitionList{5em}{
			$n$           & rozsah výběru,                \\  
			$\hat \sigma$ & výběrová směrodatná odchylka, \\  
			$IQR(x)$      & mezikvartilové rozpětí.       \\  
		}		

		Ukažme si na jednoduchém příkladu všechna tři pravidla. Vezměme data \dRef{car_fuel} a~vytvořme histogram pro spotřebu v~litrech na sto kilometrů. Vykreslíme čtyři histogramy - jeden pro každé pravidlo a~jeden pro počet intervalů\footnote{Je nutné podotknout, že jak \uv{pojmenovaná} pravidla, tak zadání čísla bere funkce \code{hist()} pouze jako orientační hodnotu. Pokud chceme přesný počet intervalů je potřeba zadat vektor s~krajními body těchto intervalů.} roven $k = \sqrt{n}$, tedy odmocnině z~počtu pozorování\footnote{Taková hodnota je výchozím nastavením například v~MS Excel.}.

		\ir{histogram_breaks}{Důležitost správné šířky intervalu pro histogram}

		Obrázek~\iRef{histogram_breaks} nám ukazuje, že menší (Sturgesův) počet intervalů pro velký výběr skutečně histogram příliš \uv{vyhladil} a zatajil tak důležitou informaci o~dvouvrcholovosti rozdělení. 

		\attributesList{histogram}{tvorbu histogramu}{
			\code{breaks} & \code{Sturges} &
				počet intervalů, do kterých jsou data rozdělena \newline
				je hned několik možností nastavení:

				\vspace{0.5em}
				\begin{enumerate}[a)]
					\item předdefinovaná pravidla: \newline \code{Sturges}, \code{Scott}, \code{Freedman-Diaconis}
					\item přirozené číslo, udávající celkový počet intervalů
					\item vektor s~body, které určují, kde se budou intervaly lámat - do těchto intervalů budou data skutečně rozdělena, předchozi dva bere funkce \code{hist()} jako přibližné
					\vspace{-1em}
					% \item funkce, která navrací přirozené číslo, udávající celkový počet skupin
				\end{enumerate}\\ \hline

			\code{freq}   & \code{TRUE} &
				\code{TRUE} - zobrazí na ose y~absolutní četnosti \newline
				\code{FALSE} - zobrazí se hodnoty jádrového odhadu (více o jádrových odhadech dále v~sekci~\myRef{sub:j_drov_odhad_hustoty}) \\
		}

		Správně zkonstruovaný histogram (tedy histogram se správnou šířkou intervalů) podává informace o~tvaru rozdělení, z~kterého můžeme přibližně vyčíst jak míry polohy, tak míry variability. Pomocí histogramu můžeme také odhalit přítomnost vybočujících pozorování. Pokud se v~histogramu vyskytuje skupina prázdných intervalů, poukazuje to na odlehlost některých z~hodnot. Takový příklad můžeme vidět na obrázku~\iRef{hist_outlier}, do kterého jsem pro názornost přidal krabičkový graf.

		\ir{hist_outlier}{Detekce odlehlých pozorování pomocí histogramu}

		Zejména u~vícenásobných grafů se nám může hodit vykreslit otočený histogram. Histogram v~R bohužel přímo takové nastavení nemá. Histogramu je potřeba přidat atribut \code{plot} s~hodnotou \code{FALSE}, čímž zamezíme vykreslení. Jeho obsah poté vykreslíme pomocí sloupcového diagramu jako na obrázku~\iRef{hist_rotated}. Jak uvidíme dále v~\myRef{sub:multi_hist}, je otočený histogram vhodný pro porovnávání dvou skupin dat mezi sebou, pro což se běžně používá v~demografii.

		\ir{hist_rotated}{Otočený histogram}

	% subsection histogram (end)

	\clearpage

	\subsection{Jádrový odhad hustoty} % (fold)
	\label{sub:j_drov_odhad_hustoty}
		Jádrový odhad je neparametrickým typem odhadu hustoty rozdělení proměnné. Je to funkce proměnné $x$ o~předpisu:
		\begin{equation}
			\hat f_h(x) = \frac{1}{nh} \sum\limits^n_{i=1} K \left( \frac{x - X_i}{h} \right)
		\end{equation}
		kde:

		\definitionList{5em}{
			$K$ & jádrová funkce, symerická kolem nuly, pro kterou platí $\int K(x)dx = 1$, \\
			$h$ & šířka vyhlazovacího okna, pro kterou platí $h > 0$. \\
		}

		Jedná se vlastně o~vážený klouzavý průměr~\cite{Řezáč2007thesis}. Jádrový odhad je vizuálně přesnější než histogram, jelikož je spojitý a~umožňuje pozorovat i~ty nejmenší změny v~průběhu rozdělení~\cite{kernel}. Stejně jako u~histogramu a~volby šířky intervalů je i~zda naprosto zásadní volba šířky vyhlazovacího okna a~volba jádrové funkce. Standardní sada jádrových funkcí je v~R definována jako: 

		\embedR{kernels_list}

		Průběh těchto funkcí jsem pro názornost vykreslil v~příloze~\iRef{kernels}. V~R provedeme jádrový odhad pomocí funkce \code{density()}, jejíž výstup použijeme jako zdroj dat pro univerzální funkci \code{plot()}. Jádrový odhad hustoty výběru z~normálního rozdělení o~rozsahu \mbox{$n = 150$} ve výchozím nastavení (tedy Gaussovskou jádrovou funkcí a~šířkou vyhlazovacího okna \code{nrd0} - zjednodušeného Scottova pravidla\footnote{Více v~\code{?bw.nrd0}.}~\cite{scott1992multivariate}.) vidíme na obrázku~\iRef{density}.

		\ir{density}{Jádrový odhad hustoty ve výchozím nastavení}

		Jak správně zvolit šířku vyhlazovacího okna stejně tak jako volbu vhodné jádrové funkce velmi detailně rozebírá Sheather~\cite{kerneldensity}. V~této práci se omezíme na jejich ilustraci. Do obrázku~\iRef{density_bw} jsem vykreslil\footnote{V~kódu jsem použil funkci \code{rug()}, která do spodní části grafu vykreslí jakousi \uv{rohož}, která znázorňuje reálné umístění hodnot proměnné. Pro přidání čar do grafu jsem použil funkci \code{lines()}, více o~ní se rozepíši v~sekci~\myRef{sub:pridani_car}.} jádrový odhad pro výběr z~normovaného normálního rozdělení o~rozsahu \mbox{$n = 150$} při použití Gaussovské jádrové funkce a~různých šířek vyhlazovacího okna.

		\ir{density_bw}{Různé šířky vyhlazovacího okna pro stejnou jádrovou funkci}

		Užití jádrového odhadu je velmi podobné jako u~histogramu~\myRef{sub:histogram}, a~proto se tyto dva odhady často vykreslují společně (za předpokladu, že histogram vykreslujeme také jako odhad hustoty - tj. s~\code{freq = FALSE}), jako na obrázku~\iRef{hist_density}.

		\ir{hist_density}{Jádrový odhad hustoty vkreslený do histogramu}

		Stejně jako histogram umožňuje jádrový odhad (při korektně zvolených parametrech) nahlédnout na rozdělení zkoumané proměnné. Z~tvaru rozdělení poznáme polohu a~variabilitu proměnné. Z~grafu můžeme vyčíst, zda se v~datech objevují odlehlá pozorování\footnote{Stejně jako u histogramu - pokud se mezi dvěma shluky dat objevuje \uv{prázdná} část grafu, poukazuje to na odlehlost jednoho ze shluků.}. Tvar rozdělení ukazuje na případné zešikmení a~na špičatost.

	% subsection j_drov_odhad_hustoty (end)
	\subsection{QQ graf} % (fold)
	\label{sub:qq_plot}
		QQ graf\footnote{Také označován jako kvantil-kvantil graf.} je graf porovnávající dvě rozdělení na základě hodnot kvantilů~\cite{qqplot}. Jednotlivé sobě-odpovídající kvantily (tj. například medián jednoho a~medián druhého rozdělení) jsou postupně vykreslovány do grafu jako body, jejichž $x$, resp. $y$ souřadnice odpovidají hodnotě kvantilů prvního, resp. druhého rozdělení.

		Pomocí QQ grafu tedy narozdíl od ostatních grafů v~této sekci neprovádíme \textit{popis} proměnné, ale \textit{porovnání} jejího rozdělení s~rozdělením jiným (buď rozdělením jiné proměnné, nebo častěji s~rozdělením teoretickým). Z~QQ grafu nepoznáme, jaké jsou míry polohy, variability či symetrie. Poznáme však, jestli se liší od měr druhého rozdělení. Poznáme také, jestli se mezi hodnotami vyskytují odlehlá pozorování.

		Nejčastěji se QQ graf používá pro porovnání s~normálním rozdělením\footnote{Zejména pak zkoumáme-li rozdělení náhodné složky v~regresní analýze.} (v R~\code{qqnorm()}), jako na obrázku~\iRef{qqnorm}, kde porovnáváme náhodný výběr z~normálního rozdělení se samotným teoretickým rozdělením.

		\ir{qqnorm}{QQ graf pro náhodný výběr z~normálního rozdělení}

		V~případě, že jsou hodnoty kvantilu přibližně stejně umístěné, leží body v~QQ grafu na přímce $y = x$. Pokud hodnoty leží na jiné přímce, stále hovoříme o~jejich lineární závislosti a~tím pádem podobnosti rozdělení. Pokud body na přímce neleží, poukazuje to na neshodu rozdělení. Jednotlivými druhy odchýlení od přímky se budeme zabývat dále.

		Pokud mezi sebou chceme proměnnou porovnat s~jiným rozdělením, než je rozdělení normální, použijeme funkci \code{qqplot()}. Porovnejme například rozdělení spotřeby automobilů z~dat~\dRef{car_fuel} s~náhodným výběrem z~rovnoměrného rozdělení o~stejném rozsahu. Na obrázku~\iRef{qqplot} pozorujeme zcela odlišný průběh rozdělení. Jedná se tedy o~ukázkový případ QQ grafu různých rozdělení. Hodnoty kvantilů se rovnají jen v~extrémech a~mediánu. Rozdělení mají tedy naprosto odlišný průběh.

		\ir{qqplot}{QQ graf pro spotřebu automobilů a~výběr z~rovnoměrného rozdělení}

		Podle tvaru QQ grafu pro normální rozdělení můžeme rozpoznat zásadní informace o~rozdělení zkoumané proměnné. Pro usnadnění můžeme použít funkci \code{qqline()}, která vykreslí přímku propojující dolní a~horní kvartil teoretického rozdělení. Ta nám umožní pozorovat celkovou vzdálenost hodnot od jejich teoretických protějšků. Také nám usnadní identifikaci odlehlých pozorování. Některé z~běžných tvarů QQ grafu jsem včetně interpretace shrnul do obrázku~\iRef{qqnorm_various}.

		\ir{qqnorm_various}{Bežné tvary QQ grafu společně s~jejich významem}

	% subsection qq_plot (end)
	\subsection{Violin plot} % (fold)
	\label{sub:violin_plot}
		Violin plot\footnote{Šlo by přeložit jako \uv{houslový graf}, ale nepřekládá se.} je kombinací krabičkového grafu a~jádrového odhadu hustoty~\cite{violinplot}. Spojuje pozitivní vlastnosti obou grafů a~umožňuje nám pracovat jen s~jedním grafem namísto dvou. Odstraňuje dříve zmíněné nevýhody samostatného použití těchto grafů.

		Pro R~existuje \textbf{vioplot} balíček\footnote{Více informací o~balíčku lze získat na \href{http://cran.r-project.org/web/packages/vioplot/index.html}{http://cran.r-project.org/web/packages/vioplot/index.html}.}~\cite{Rvioplot} s~funkcí \code{violinplot()}, vytvářející tento typ grafu. Na obrázku~\iRef{vioplot} vidíme violin plot ve výchozím nastavení. Je samozřejmě velmi podobný krabičkovému grafu. Tečka udává polohu mediánu, délka \uv{krabičky} pak mezikvartilové rozpětí. Barevná plocha vyjadřuje křivku jádrového odhadu hustoty rozdělení.

		\ir{vioplot}{Violin plot}

		Rozíření o~jádrový odhad je naprosto zásadní a~řeší problém, který znázorňuji na obrázku~\iRef{boxplot_vs_reality}. Udělejme nyní obdobný obrázek - jen namísto krabičkového grafu použijeme violin plot. Výsledek na obrázku~\iRef{vioplot_vs_reality} nám ukazuje, že violin plot umí spolehlivě rozlišit různé tvary rozdělení.

		\ir{vioplot_vs_reality}{Pomocí violin plotu dokážeme na rozdíl od krabičkového grafu rozeznat různá rozdělení}

		\clearpage

		Na obrázku~\iRef{vioplot_vs_boxplot} je vidět, kolik informací navíc nám o~zkoumané proměnné violin plot oproti krabičkovému grafu dává. 

		\ir{vioplot_vs_boxplot}{Violin plot nám dává daleko více informací o~rozdělení proměnné než krabičkový graf}

		Z~violin plotu vyčteme veškeré podstatné informace o~proměnné. Přimo vidíme hodnoty měr polohy, z~tvaru rozdělení a~velikosti mezikvartilového rozpětí vidíme variabilitu a~případnou asymetrii. Violin plot je proto dle mého názoru nejlepším nástrojem pro popis spojité kvantitativní proměnné.
	% subsection violin_plot (end)

	\subsection{Grafy pro kategoriální proměnné} % (fold)
	\label{sub:dal_zaj_mav_grafy}
		\subsubsection{Výsečový graf} % (fold)
		\label{ssub:kol_ov_graf}
			V~případě, že zkoumáme kategoriální proměnnou, můžeme pro základní popis využít výsečový graf. Výsečový graf nám poskytuje rychlý přehled o~relativních četnostech kategorií. Právě proto, že ukazuje relativní zastoupení, může být výsečový graf velmi zavádějící. Vždy bychom k~němu měli dodávat, z~jak velkého vzorku je tvořen a~na jak velkou populaci se snažíme zobecnit výsledek. Ve výsečovém grafu nejsou rozsah výběru ani velikost populace vidět. Na to se mnohdy zapomíná a~dochází k~nesmyslným závěrům nad nedostatečným počtem pozorováním.

			Podívejme se například na zastoupení značek automobilů v~datech \dRef{car_fuel}. Pro výsečový graf musíme vypočítat absolutní četnosti, ke kterým přiřadíme náležící názvy. Pokud je jako v~případě těchto dat mnoho kategorií, je vhodné (například porovnáním četností kategorií s~mediánem četností) zobrazit popisky jen u nejčetnějších názvů. Výsečový graf vidíme na obrázku~\iRef{pie_better}.

			\ir{pie_better}{Výsečový graf pro četnosti výrobců automobilů}

			Výsečový graf se pro výše zmíněný problém používá spíše v~nestatistické literatuře.
		% subsubsection kol_ov_graf (end)
		\subsubsection{Wordcloud} % (fold)
		\label{ssub:wordcloud}
			Spíše pro zajímavost přidávám zejména na internetu oblíbené zobrazení četnosti slov - word\-cloud\footnote{Též tag cloud; ani jeden termín se do češtiny nepřekládá.}. Nalezená slova jsou tisknuta proporčně k~jejich četnosti v~textu a~uspořádána do jakéhosi \uv{oblaku}. To nám umožňuje již na první pohled vidět, které hodnoty jsou nejčetnější. Využitím a~významem je to vlastně obdoba výsečového grafu, jen s~jiným grafickým znázorněním.

			Nejjednodušší cestou k~vytvoření tohoto zobrazení v~R je použití balíčku \textbf{wordcloud}~\cite{wordcloud}. Ten obsahuje stejnojmennou funkci. Zápis je obdobný jako u~výsečového grafu.

			\ir{wordcloud}{Wordcloud}

			Na příkladu wordcloudu na obrázku~\iRef{wordcloud} jsem použil zajímavou funkci \code{rainbow}, umožňující tvorbu vektoru s~barvami duhy. Více o~této funkci v~nápovědě \code{?rainbow}.

		% subsubsection wordcloud (end)
	% subsection dal_zaj_mav_grafy (end)
% section 1d (end)