\select@language {czech}
\select@language {english}
\select@language {czech}
\contentsline {section}{\numberline {1}Předmluva}{1}{section.1}
\contentsline {section}{\numberline {2}Úvod do R}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}O systému R}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Práce s~R}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Grafy v~R}{4}{subsection.2.3}
\contentsline {section}{\numberline {3}Jednorozměrná data}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Krabičkový graf}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Histogram}{9}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Jádrový odhad hustoty}{13}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}QQ graf}{15}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Violin plot}{18}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Grafy pro kategoriální proměnné}{20}{subsection.3.6}
\contentsline {subsubsection}{\numberline {3.6.1}Výsečový graf}{20}{subsubsection.3.6.1}
\contentsline {subsubsection}{\numberline {3.6.2}Wordcloud}{21}{subsubsection.3.6.2}
\contentsline {section}{\numberline {4}Dvourozměrná data}{23}{section.4}
\contentsline {subsection}{\numberline {4.1}XY graf}{23}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Bagplot}{24}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Vícenásobný krabičkový graf}{25}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Sectioned density plot}{28}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Histogramy}{30}{subsection.4.5}
\contentsline {section}{\numberline {5}Vícerozměrná data}{32}{section.5}
\contentsline {subsection}{\numberline {5.1}Rainbow plot}{32}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Interaktivní 3D graf}{33}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Mozaikový graf}{35}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Vizualizace korelační matice}{36}{subsection.5.4}
\contentsline {section}{\numberline {6}Univerzální nastavení a~úpravy grafů}{38}{section.6}
\contentsline {subsection}{\numberline {6.1}Kombinování grafů}{38}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Více grafů v~jednom obrázku}{38}{subsubsection.6.1.1}
\contentsline {subsection}{\numberline {6.2}Přidání bodů do grafu}{39}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Přidání čar do grafu}{39}{subsection.6.3}
\contentsline {subsubsection}{\numberline {6.3.1}Přidání přímky}{39}{subsubsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.2}Přidání grafu dle předpisu funkce}{41}{subsubsection.6.3.2}
\contentsline {subsubsection}{\numberline {6.3.3}Přidání specifických čar}{41}{subsubsection.6.3.3}
\contentsline {section}{\numberline {7}Vytvořené funkce}{44}{section.7}
\contentsline {subsection}{\numberline {7.1}Rainbow plot funkce}{44}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Funkce pro tvorbu stromu života}{45}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Funkce popisující vznik sectioned density plotu}{46}{subsection.7.3}
\contentsline {section}{\numberline {8}Závěr}{47}{section.8}
\contentsline {section}{\numberline {9}Reference}{48}{section.9}
\contentsline {section}{\numberline {A}Příloha}{51}{appendix.A}
\contentsline {subsection}{\numberline {A.1}Použitá data}{51}{subsection.A.1}
\contentsline {subsubsection}{\numberline {A.1.1}Spotřeba automobilů 2012}{51}{subsubsection.A.1.1}
\contentsline {subsubsection}{\numberline {A.1.2}Produkce plynu v~Austrálii}{51}{subsubsection.A.1.2}
\contentsline {subsubsection}{\numberline {A.1.3}Návštěvnost ČR}{52}{subsubsection.A.1.3}
\contentsline {subsubsection}{\numberline {A.1.4}Míry úmrtnosti v~ČR}{52}{subsubsection.A.1.4}
\contentsline {subsubsection}{\numberline {A.1.5}Střední stav populace ČR}{53}{subsubsection.A.1.5}
\contentsline {subsubsection}{\numberline {A.1.6}Barva očí a~vlasů}{53}{subsubsection.A.1.6}
\contentsline {subsubsection}{\numberline {A.1.7}Údaje o~automobilech}{54}{subsubsection.A.1.7}
\contentsline {subsection}{\numberline {A.2}Grafy}{54}{subsection.A.2}
