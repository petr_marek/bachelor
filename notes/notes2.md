# Grafické zobrazení statistických dat

*Bakalářská práce se bude zabývat metodami grafického zobrazení jednorozměrných, dvourozměrných a třírozměrných statistických dat (kvantitativních i kvalitativních). Práci se doporučuje zpracovat ve statistickém softwaru R (jeho znalost v okamžiku zapsání práce však není nutná - student se s tímto softwarem naučí pracovat v průběhu zpracování bakalářské práce).*


## Smysl prace

Ctenar bude schopen spravne zobrazit data v R. Podle charakteru dat vybere vhodny graf a pomoci R jej vytvori a bude schopen dale prizpusobovit.

## Struktura

1. Strucny uvod o R
2. Jeste strucneji import dat a zaklady syntaxe
3. Zpracovani jednotlivych druhu dat, vzdy s priklady na urcitych datech
	+ jednorozmerne
		+ krabickove grafy
		+ histogramy
		+ sloupcove
		+ kolacove grafy
	+ dvourozmerne
		+ bodove/carove grafy (casove rady aj)
		+ regrese - napr. teple/studene barvy viz. zajimave grafy
	+ trirozmerne
		+ interaktivni 3d graf
		+ zavislosti mezi jednotlivymi promennymi - vsemozne gridy grafu
3. Zajimave/uzitecne grafy
	+ napr. demograficky strom zivota a jine

napric praci zobrazovat relevantni moznosti:

+ orientace grafu a barvy car/bodu
+ psani do grafu
+ pridavani jednotlivych bodu/car
+ pozicovani a nastaveni legendy
+ nastaveni os

## K obsahu

Prace by mela byt jako "R kucharka". Pro jednotlive funkce vzdy uvedu zakladni syntax, popisu relevantni parametry a jejich mozne hodnoty. Rozdilne nastaveni grafu budu ilustrovat na obrazkovych prikladech.

## Dalsi poznamky
+ uzitecne grafy:
	+ qq plot
+ zajimave grafy:
	+ demograficky strom zivota
	+ sila zavislosti pomoci teplych/studenych barev na "spreadsheetu"
	+ ruzne variace na boxplot
	+ word cloud - neco jako http://gallery.r-enthusiasts.com/graph/Word_Cloud_162 - cetnost urcuje velikost textu
