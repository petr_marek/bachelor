# Grafické zobrazení statistických dat

*Bakalářská práce se bude zabývat metodami grafického zobrazení jednorozměrných, dvourozměrných a třírozměrných statistických dat (kvantitativních i kvalitativních). Práci se doporučuje zpracovat ve statistickém softwaru R (jeho znalost v okamžiku zapsání práce však není nutná - student se s tímto softwarem naučí pracovat v průběhu zpracování bakalářské práce).*

## Literatura (v EN)

+ Data analysis graphics using R
+ Introductory Statistics with R - horsi
+ bonus - Deepayan Sarkar - Lattice

## Dalsi

+ psat v anglictine?
+ v cem psat?
	+ tech?, spis obyc office


## Poznamky
+ Rseek


## Provedeni
+ popis fci nastaveni, uzivatel prizpusobeni grafu, bez navazani na statistiku, vcetne ilustraci
+ navazani funkci na statisticke metody
+ prakticke priklady a aplikace? vlastni funkce na graf?
